import axios from "axios";
export default axios.create({
  baseURL: "https://www.thesportsdb.com/api/v1/json/2/all_sports.php",
  headers: {
    "Content-type": "application/json"
  }
});