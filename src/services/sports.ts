import http from "../http-common";
// import SportsData from "../types/sports"

class SportsDataService {
  getAll() {
    return http.get("/");
  }
}
export default new SportsDataService();