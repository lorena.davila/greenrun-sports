import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getFirestore, CollectionReference, collection, DocumentData } from 'firebase/firestore'
import SavedSport from '../../types/savedSport';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID,
};

export const Firebase = firebase.initializeApp(firebaseConfig);
export const firebaseDB = getFirestore();
// Add or Remove authentification methods here.
export const Providers = {
  email: new firebase.auth.EmailAuthProvider(),
};

export const auth = firebase.auth();
// export default Firebase;
// This is just a helper to add the type to the db responses
const createCollection = <T = DocumentData>(collectionName: string) => {
  return collection(firebaseDB, collectionName) as CollectionReference<T>
}

export const sportsCol = createCollection<SavedSport>('savedSports')
