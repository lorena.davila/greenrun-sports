import { Route, Routes } from 'react-router-dom';
import History from './pages/History';
import Home from './pages/Home';
import Login from './pages/Login';
import GlobalStyle from './components/GlobalStyles';
import Onboarding from './pages/Onboarding';
import SignUp from './pages/SignUp';
import PrivateRoute from "./components/PrivateRoute";
import { lightTheme, darkTheme } from "./components/Themes"
import { ThemeProvider } from "styled-components";
import { useProvider } from './app-context';

function App() {
  const { state } = useProvider();

  return (
      <ThemeProvider theme={state.theme === 'light' ? lightTheme : darkTheme}>
        <GlobalStyle />
        <Routes>
          <Route path="/" element={<Onboarding />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path='/sports' element={<PrivateRoute />}>
            <Route path='/sports' element={<Home />} />
          </Route>
          <Route path='/history' element={<PrivateRoute />}>
            <Route path='/history' element={<History />} />
          </Route>
        </Routes>
      </ThemeProvider >
  );
}

export default App;
