import * as React from 'react';
import SportData from './types/sport';
import SavedSport from './types/savedSport';

type Action = { type: string, payload: any };
type Dispatch = (action: Action) => void;
type State = { user: { email: string, token: string }, sports: SportData[], currentSport: number, savedSports: SavedSport[], theme: string };
type AppProviderProps = { children: React.ReactNode }

const AppStateContext = React.createContext<
  { state: State; dispatch: Dispatch } | undefined
>(undefined)

function appReducer(state: State, action: Action) {
  switch (action.type) {
    case 'sign':
      return { ...state, user: action.payload };
    case 'fetchSports':
      return { ...state, sports: action.payload };
    case 'saveSport':
      return { ...state, savedSports: [...state.savedSports, action.payload] };
    case 'setTheme':
      return { ...state, theme: state.theme === 'dark' ? 'light' : 'dark' };
    case 'setCurrentSport':
      //increment currentSport while sports still has items
      if (state.sports.length > action.payload)
        return { ...state, currentSport: action.payload };
      else return { ...state, currentSport: -1 };
    default:
      throw new Error(`Unhandled action type: ${action.type}`)
  }
}

function AppProvider({ children }: AppProviderProps) {
  const [state, dispatch] = React.useReducer(appReducer, { user: {}, sports: [], currentSport: 0, savedSports: [], theme: 'dark' });
  const value = { state, dispatch }
  return <AppStateContext.Provider value={value}>{children}</AppStateContext.Provider>
}

function useProvider() {
  const context = React.useContext(AppStateContext)
  if (context === undefined) {
    throw new Error('useProvider must be used within a AppProvider')
  }
  return context
}

export { AppProvider, useProvider }

