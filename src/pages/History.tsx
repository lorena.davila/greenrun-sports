import React, { useEffect } from 'react';
import styled from 'styled-components';
import NavBar from '../components/NavBar';
import { Header2, Header3, Paragraph } from '../components/Typography';
import arrowBack from '../assets/images/History/arrow-back.svg';
import { Link } from 'react-router-dom';
import { useProvider } from '../app-context';
import HistoryItem from '../components/HistoryItem';
import SavedSport from '../types/savedSport';

const HistoryBox = styled.div`
  margin: 5.4rem 3.2rem 14.5rem;
`;

const BackButton = styled.div`

`;

const NoSports = styled.div`
  margin: 5.4rem 3.2rem;
`;

const DateText = styled.span`
  font-size: 1.4rem;
  line-height: 2.1rem;
  color: ${({ theme }:any) =>  theme.textSecondary };
`;

const History = () => {
  const { state } = useProvider();
  const [isLoading, setIsLoading] = React.useState(true);
  const [sportsByDate, setSportsByDate] = React.useState({} as { [key: string]: any });

  const handleLoading = () => {
    setIsLoading(false);
  }

  // type SportsByDate = { name: string, image: string, liked: boolean } };

  useEffect(() => {
    const sportsByDate = {} as any;
    state.savedSports.forEach((sport: SavedSport) => {
      let day = new Intl.DateTimeFormat('en', { day: 'numeric' }).format(sport.date);
      let month = new Intl.DateTimeFormat('en', { month: 'long' }).format(sport.date);
      const date = `${day} ${month}`;

      if (!sportsByDate[date]) {
        sportsByDate[date] = [];
      }
      sportsByDate[date].push(sport);
    });
    setSportsByDate(sportsByDate);

    handleLoading();
  }, []);

  return (
    <>
      <HistoryBox>
        <Link to="/sports">
          <BackButton>
            <img src={arrowBack} alt="Go Back" />
          </BackButton>
        </Link>
        <Header2>History</Header2>
        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        {
          isLoading ?
            ''
            :
            state.savedSports.length > 0 ?
              Object.entries(sportsByDate).map(([date, sports], index) =>
                <div key={index}>
                  <DateText>{date}</DateText>
                  {
                    sports.map((sport: SavedSport, indexSport: number) => {
                      const sportData = state.sports[sport.sportIndex];
                      return <HistoryItem key={indexSport} name={sportData.name} image={sportData.image} liked={sport.liked} />
                    })
                  }
                </div>
              )
              :
              <NoSports>
                <Header3>There are no sports in your History yet</Header3>
              </NoSports>
        }
      </HistoryBox>

      <NavBar />
    </>
  )
}

export default History