import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components'
import { useProvider } from '../app-context';
import SignButton from '../components/LoginBtn';
import { Header2, InputLabel, Paragraph } from '../components/Typography';
import { auth } from '../services/firebase/firebaseSetup';

const TopBox = styled.div`
  margin-top: 20.2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ParagraphBox = styled.div`
  width: 32.5rem;
  text-align: center;
  margin-bottom: 2.3rem;
  opacity: 0.8;
`;

const InputBox = styled.div`
  padding-top: 1rem;
  padding-left: 1.6rem;
  padding-bottom: 0.8rem;
  background: #2F2F43;
  border: 1px solid rgba(255, 255, 255, 0.06);
  box-sizing: border-box;
  border-radius: 18px;
  width: 329px;
  height: 67px;
  display: flex;
  flex-direction: column;
  margin-bottom: 1rem;
`;

const Input = styled.input`
  background: transparent;
  border: none;
  font-family: 'DM Sans Regular';
  font-size: 1.8rem;
  line-height: 2.2rem;
  padding-top: .5rem;
  color: #FEFEFE;
`;

const ForgotPass = styled.span`
  font-family: 'DM Sans Regular';
  font-size: 1.6rem;
  line-height: 2rem;
  color: rgba(254, 254, 254, 0.8);
  margin-top: 1rem;
  margin-bottom: 2.2rem;
`;

const BottomBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  width: 329px;
`;

const SignUp = () => {
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);
  const { dispatch } = useProvider();
  let navigate = useNavigate();

  const createAccount = async () => {
    try {
      const signToken = await auth.createUserWithEmailAndPassword(
        emailRef.current!.value,
        passwordRef.current!.value
      );
      //Store user variable in context
      dispatch({ type: 'sign', payload: { user: { email: emailRef.current!.value, token: signToken}} });
      navigate('/sports');
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <TopBox>
        <Header2>Welcome</Header2>
        <ParagraphBox>
          <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        </ParagraphBox>
        <InputBox>
          <InputLabel>User</InputLabel>
          <Input ref={emailRef} type="text" placeholder="usuario@greenrun.com" />
        </InputBox>
        <InputBox>
          <InputLabel>Password</InputLabel>
          <Input ref={passwordRef} type="password" placeholder="****************" />
        </InputBox>

        <BottomBox>
          <ForgotPass></ForgotPass>
          <SignButton styles={{}} name="SignUp" handler={createAccount} />
        </BottomBox>
      </TopBox>
    </>
  )
}

export default SignUp