import styled from 'styled-components'
import LoginBtn from '../components/LoginBtn';
import messiCover from '../assets/images/Onboarding/messi-cover.png'
import { Link } from 'react-router-dom'
import { Header1, Paragraph } from '../components/Typography';

const CallToAction = styled.div`
    padding-top: 4.6rem;
    padding-left: 3.2rem;
    width: 390px;
    height: 346px;
    background: ${({ theme }:any) =>  theme.boxColor };
    border-radius: 36px 36px 0px 0px ;
    position: absolute;
    bottom: 0;
    left: 0;
  `;

const Image = styled.img`
  position: absolute;
  left: -5px;
  top: 35px;
  `;

const HeaderBox =styled.div`
  width: 25.2rem;
  color: ${({ theme }:any) =>  theme.textSecondary };
`;

const DescriptionBox =styled.div`
  width: 325px;
  margin-bottom: 5.4rem;
`;

const ButtonsBox = styled.div`
`;

const Onboarding = () => {
  return (
    <div>
      <Image src={messiCover} alt="Lionel Messi" />
      <CallToAction>
        <HeaderBox>
          <Header1>Discover Your Best Sport With Us</Header1>
        </HeaderBox>
        <DescriptionBox>
          <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        </DescriptionBox>
        <ButtonsBox>
          <Link to="/login"><LoginBtn styles={{}} name="Login" handler={()=>{}} /></Link>
          <Link to="/signup"><LoginBtn styles={{'margin-left':'2rem'}} name="Signup" handler={()=>{}} /></Link>
        </ButtonsBox>
      </CallToAction>
    </div>
  )
}

export default Onboarding