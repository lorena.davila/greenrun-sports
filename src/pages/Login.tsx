import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import SignButton from '../components/LoginBtn';
import { Header2, InputLabel, Paragraph } from '../components/Typography';
import { auth } from '../services/firebase/firebaseSetup';
import { useProvider } from '../app-context';
import { useNavigate } from 'react-router-dom';
import { sportsCol } from '../services/firebase/firebaseSetup';
import { getDocs } from '@firebase/firestore';
import SportsDataService from '../services/sports';

const TopBox = styled.div`
  margin-top: 20.2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ParagraphBox = styled.div`
  width: 32.5rem;
  text-align: center;
  margin-bottom: 2.3rem;
  opacity: 0.8;
`;

const InputBox = styled.div`
  padding-top: 1rem;
  padding-left: 1.6rem;
  padding-bottom: 0.8rem;
  background:  ${({ theme }:any) =>  theme.input };
  border: 1px solid rgba(255, 255, 255, 0.06);
  box-sizing: border-box;
  border-radius: 18px;
  width: 329px;
  height: 67px;
  display: flex;
  flex-direction: column;
  margin-bottom: 1rem;
`;

const Input = styled.input`
  background: ${({ theme }:any) =>  theme.input };
  border: none;
  font-family: 'DM Sans Regular';
  font-size: 1.8rem;
  line-height: 2.2rem;
  padding-top: .5rem;
  color: ${({ theme }:any) =>  theme.inputColor };
`;

const ForgotPass = styled.span`
  font-family: 'DM Sans Regular';
  font-size: 1.6rem;
  line-height: 2rem;
  color: ${({ theme }:any) =>  theme.inputColor };
  margin-top: 1rem;
  margin-bottom: 2.2rem;
`;

const Error = styled.span`
  font-family: 'DM Sans Regular';
  font-size: 1.6rem;
  line-height: 2rem;
  color: rgba(235, 144, 144, 0.8);
  margin-top: 1rem;
  margin-bottom: 2.2rem;
`;

const BottomBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  width: 329px;
`;

const Login = () => {
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);
  const { dispatch } = useProvider();
  const [error, setError] = React.useState('');
  let navigate = useNavigate();

  const getSports = async () => {
    const response = await SportsDataService.getAll()

    if (response.status === 200) {
      const sports = response.data.sports.map((sport: any) => {
        return {
          id: sport.idSport,
          name: sport.strSport,
          icon: sport.strSportIconGreen,
          image: sport.strSportThumb
        }
      });
      dispatch({ type: 'fetchSports', payload: sports });
    } else {
      console.log(response);
    }
  }

  const getSavedSports = async () => {
    const savedSports = await getDocs(sportsCol);

    // filter by user email, this is temporal until the query can be filtered
    const userSports = savedSports.docs.filter(
      (s) => {
        const sport = s.data();
        return sport?.user === emailRef.current?.value;
      }
    );

    userSports.forEach((s) => {
      const sport = s.data();
      dispatch({ type: 'saveSport', payload: sport });
    })
  };

  const signIn = async () => {
    try {
      const response = await auth.signInWithEmailAndPassword(
        emailRef.current!.value,
        passwordRef.current!.value
      );
      //Store user variable in context
      const token = await response?.user?.getIdToken();
      dispatch({ type: 'sign', payload: { email: emailRef.current!.value, token: token } });
      await getSavedSports();
      await getSports();

      navigate('/sports');
    } catch (error: any) {
      console.error(error);
      setError(error.message);
    }
  };

  return (
    <>
      <TopBox>
        <Header2>Welcome</Header2>
        <ParagraphBox>
          <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        </ParagraphBox>
        <InputBox>
          <InputLabel>User</InputLabel>
          <Input ref={emailRef} type="text" placeholder="usuario@greenrun.com" />
        </InputBox>
        <InputBox>
          <InputLabel>Password</InputLabel>
          <Input ref={passwordRef} type="password" placeholder="****************" />
        </InputBox>
        <BottomBox>
          <ForgotPass>Forgot your Password?</ForgotPass>
          <Error>{error}</Error>

          <SignButton styles={{}} name="Login" handler={signIn} />
        </BottomBox>
      </TopBox>
    </>
  )
}

export default Login
