import React, { useEffect } from 'react'
import { useProvider } from '../app-context';
import lightMode from '../assets/images/Home/light-mode.svg';
import NavBar from '../components/NavBar';
import { Header3 } from '../components/Typography';
import SportsData from '../types/sport';
import styled from 'styled-components'
import dislike from '../assets/images/Home/dislike-white.svg';
import like from '../assets/images/Home/heart-white.svg';
import { sportsCol } from '../services/firebase/firebaseSetup';
import { doc, setDoc } from '@firebase/firestore'
import { useTransition, animated, config } from 'react-spring'

const Home = () => {
  const { state, dispatch } = useProvider();
  const [currentSport, setCurrentSport] = React.useState({} as SportsData);
  const [likeAnimationStatus, likeDisplayAnimation] = React.useState(false);
  const [dislikeAnimationStatus, dislikeDisplayAnimation] = React.useState(false);
  const [imageAnimationStatus, imageDisplayAnimation] = React.useState(false);

  const themeToggler = () => {
    dispatch({ type: 'setTheme', payload: "" });
  }

  const likeAnimationProps = useTransition(likeAnimationStatus, {
    enter: { position: 'absolute', top: '50%', left: '50%', opacity: 1 },
    config: config.molasses,
    delay: 100,
  });

  const  imageAnimationProps = useTransition(imageAnimationStatus, {
    enter: { width: '100vw', height: '100vh', opacity: 1 },
    config: config.molasses,
    delay: 100,
  });

  async function saveSport(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): Promise<void> {
    const liked = e.currentTarget.id === 'like' ? true : false;
    const savedSport = { sportIndex: state.currentSport, liked, user: state.user.email, date: new Date() };
    dispatch({ type: 'saveSport', payload: savedSport });
    const sportRef = doc(sportsCol, 'sports-' + state.user.email + "-" + state.currentSport);
    await setDoc(sportRef, savedSport);
    
    likeDisplayAnimation(liked);
    dislikeDisplayAnimation(!liked);
    imageDisplayAnimation(true);

    setTimeout(() => {
      dispatch({ type: 'setCurrentSport', payload: state.currentSport + 1 });
      likeDisplayAnimation(false);
      dislikeDisplayAnimation(false);
      imageDisplayAnimation(false);
    }, 1000);
  }

  useEffect(() => {
    if (state.currentSport !== -1)
      setCurrentSport(state.sports[state.currentSport]);
    else
      setCurrentSport({} as SportsData);
  }, [state.currentSport])

  useEffect(() => {
      dispatch({ type: 'setCurrentSport', payload: state.savedSports.length });
  }, []);

  const SportBox = styled.div`
    position: relative;
  `;

  const LightModeBtn = styled.button`
    cursor: pointer;
    width: 6.2rem;
    height: 6.3rem;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    left: 2.3rem;
    top: 2.2rem;
    background: #222243;
    backdrop-filter: blur(20px);
    /* Note: backdrop-filter has minimal browser support */
    border: none;
    border-radius: 1.8rem;
  `;

  const IconBox = styled.div`
    position: absolute;
    right: 2.3rem;
    top: 2.2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 6.2rem;
    height: 6.3rem;
    background: rgba(34, 34, 67, 0.2);
    backdrop-filter: blur(20px);
    /* Note: backdrop-filter has minimal browser support */
    border-radius: 1.8rem;
  `;

  const SportImage = styled.img`
    width: 39rem;
    height: 56.5rem;
    border-bottom-left-radius: 3.2rem;
    border-bottom-right-radius: 3.2rem;
  `;

  const GradientBox = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    width: 39rem;
    height: 10rem;
    background: linear-gradient(360deg, #000000 0%, #000000 58.85%, rgba(0, 0, 0, 0) 100%);
    border-radius: 0 0 3.2rem 3.2rem;
  `;

  const HeaderBox = styled.div`
    position: absolute;
    bottom: 2.1rem;
    left: 2.6rem;
  `;

  const ButtonsBox = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 4.7rem;
    margin-bottom: 15.2rem;
  `;

  const DislikeButton = styled.button`
    margin-right: 2.2rem;
    background: #222243;
    box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.08);
    width: 5.1rem;
    height: 5.1rem;
    border-radius: 50%;
    border: none;
    cursor: pointer;
 `;

  const LikeButton = styled.button`
    background: linear-gradient(125.02deg, #236BFE -17.11%, #063BA8 98.58%);
    box-shadow: 0px 10px 25px rgba(35, 107, 254, 0.2);
    width: 8.1rem;
    height: 8.1rem;
    border-radius: 50%;
    border: none;
    cursor: pointer;
  `;

  const NoSports = styled.div`
    margin: 5.4rem 3.2rem;
  `;

  return (
    <div>
      {
        (state.currentSport !== -1) ?
          <div >
            <SportBox>
              <LightModeBtn onClick={themeToggler}>
                <img src={lightMode} alt="Light Mode" />
              </LightModeBtn>
              <IconBox>
                <img src={currentSport?.icon} width="26px" height="38px" alt="Sports Icon" />
              </IconBox>

              {!imageAnimationProps ? (
                <SportImage src={currentSport?.image} alt={currentSport?.name} />
              ) : (
                likeAnimationProps(
                  ({ opacity }, item) => {
                    return <animated.div style={{
                      transform: opacity
                        .to((y: any) => `width: 100vw; height: 100vh;`),
                    }}>
                      <SportImage src={currentSport?.image} alt={currentSport?.name} />
                    </animated.div>
                  }
                ))}

              {/* <SportImage src={currentSport?.image} alt={currentSport?.name} /> */}
              <GradientBox>
                <HeaderBox>
                  <Header3>{currentSport?.name}</Header3>
                </HeaderBox>
              </GradientBox>
            </SportBox>
            <ButtonsBox>

              {!dislikeAnimationStatus ? (
                <DislikeButton id="dislike" onClick={e => saveSport(e)} >
                  <img src={dislike} alt="Dislike" />
                </DislikeButton>
              ) : (
                likeAnimationProps(
                  ({ opacity }, item) => {
                    return <animated.div style={{
                      transform: opacity
                        .to((y: any) => `translate3d(${50}%,-${320}px,0)`),
                    }}>
                      <DislikeButton id="dislike" >
                        <img src={dislike} alt="Dislike" />
                      </DislikeButton>
                    </animated.div>
                  }
                ))}

              {!likeAnimationStatus ? (
                <LikeButton id="like" onClick={e => saveSport(e)} >
                  <img src={like} alt="Like" />
                </LikeButton>
              ) : (
                likeAnimationProps(
                  ({ opacity }, item) => {
                    return <animated.div style={{
                      transform: opacity
                        .to((y: any) => `translate3d(-${50}%,-${320}px,0)`),
                    }}>
                      <LikeButton id="like">
                        <img src={like} alt="Like" />
                      </LikeButton>
                    </animated.div>
                  }
                )
              )}
            </ButtonsBox>
          </div>
          :
          <NoSports>
            <Header3>It seems there are no more sports left :)</Header3>
          </NoSports>
      }
      <NavBar />
    </div >
  )
}

export default Home
