import styled from 'styled-components';
// import { auth } from '../services/firebase/firebaseSetup';
import home from '../assets/images/Home/home.svg';
import history from '../assets/images/Home/history.svg';
import notes from '../assets/images/Home/notes.svg';
import profile from '../assets/images/Home/profile.svg';
import { Link } from 'react-router-dom';

const Nav = styled.nav`
  width: 98%;
  display: flex;
  align-items: center;
  justify-content: space-around;

  height: 85px;
  background: ${({ theme }:any) =>  theme.navColor };
  border-radius: 24px;
  position: fixed;
  bottom: 2rem;
  left: 1%;
`;

const List = styled.ul`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const ListItem = styled.li`
  cursor: pointer;
`;

const NavBar = () => {
  // const signOut = async () => {
  //   await auth.signOut();
  // };

  return (
    <Nav>
      <List>
        <Link to="/sports"><ListItem><img src={home} alt="home" /></ListItem></Link>
        <Link to="/history"><ListItem><img src={history} alt="history" /></ListItem></Link>
        <Link to="/notes"><ListItem><img src={notes} alt="notes" /></ListItem></Link>
        <Link to="/profile"><ListItem><img src={profile} alt="profile" /></ListItem></Link>
        {/* <ListItem><img src={} alt="sign out"/></ListItem> */}
      </List>
    </Nav>
  )
}

export default NavBar