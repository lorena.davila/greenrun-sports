import React from 'react'
import styled from 'styled-components';
import dislike from '../assets/images/History/dislike-red.svg';
import like from '../assets/images/History/heart.svg';
import { Header4 } from './Typography';

const ItemBox = styled.div`
  width: 32.6rem;
  height: 7.7rem;
  background: #212135;
  border-radius: 1.2rem;
  margin-top: 1.4rem;
  display: flex;
  position: relative;
`;

const SportImage = styled.img`
  width: 25.8rem;
  height: 7.7rem;
  background: rgba(0, 0, 0, 0.51);
  border-radius: 12px;
`;

const IconBox = styled.div`
  display: flex;
  align-items: center;
  margin-left: 2rem;
  /* justify-content: flex-end; */
`;

const HeaderBox = styled.div`
  position: absolute;
  top: 2.4rem;
  left: 1.5rem;
`;

type Props = { name: string, image: string, liked: boolean };

const HistoryItem = ({ name, image, liked }: Props) => {
  return (
    <ItemBox>
      <SportImage src={image} alt={name} />
      <HeaderBox>
        <Header4>{name}</Header4>
      </HeaderBox>
      <IconBox>
        {
          liked ? <img src={like} alt="Liked" /> : <img src={dislike} alt="Disliked" />
        }
      </IconBox>
    </ItemBox>
  )
}

export default HistoryItem
