import styled from 'styled-components';


export const Header1 = styled.h1`
  font-family: 'DM Sans Bold';
  font-size: 2.8rem;
  line-height: 3.4rem;
  color: ${({ theme }:any) =>  theme.textSecondary };
`;

export const Header2 = styled.h2`
  font-family: 'DM Sans Bold';
  font-size: 4.2rem;
  line-height: 5.1rem;
  color: ${({ theme }:any) =>  theme.textSecondary };
`;

export const Header3 = styled.h3`
  font-family: 'DM Sans Bold';
  font-size: 3.4rem;
  line-height: 4.1rem;
  color: #FEFEFE;
`;

export const Header4 = styled.h4`
  font-family: 'DM Sans Bold';
  font-size: 2.4rem;
  line-height: 2.9rem;
  color: #FEFEFE;
`;

export const Paragraph = styled.p`
  font-family: 'Epilogue';
  font-size: 1.8rem;
  line-height: 2.7rem;
`;

export const InputLabel = styled.span`
  font-family: 'DM Sans bold';
  font-size: 1.4rem;
  line-height: 1.7rem;
  color: ${({ theme }:any) =>  theme.inputColor };
  opacity: 0.6;
`;
