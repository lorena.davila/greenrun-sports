export const darkTheme = {
  body: '#181828',
  text: '#161617',
  textSecondary: '#FEFEFE',
  paragraph: '#FEFEFE',
  input: 'transparent',
  inputColor: '#FEFEFE',
  boxColor: '#181828',
  navColor: '#2C2B3E',
}

export const lightTheme = {
  body: '#E5E5E5',
  text: '#FEFEFE',
  textSecondary: '#161617',
  paragraph: '#232232',
  input: '#FFF',
  inputColor: '#161617',
  boxColor: '#FFFFFF',
  navColor: '#FFFFFF',
}

