import React from 'react'
import styled from 'styled-components';

const SignBtn = styled.button`
  width: 122px;
  height: 66px;
  background: linear-gradient(99deg, #236BFE 6.69%, #0D4ED3 80.95%);
  box-shadow: 0px 4px 30px rgba(34, 105, 251, 0.8);
  border-radius: 25px;
  border: none;
  cursor: pointer;

  font-family: 'DM Sans Bold';
  font-size: 1.8rem;
  line-height: 2.2rem;
  color: #FEFEFE;
`;
type Props = { name: string, handler: any, styles: Object };

const defaultStyle = {
  styles: {}
};


const SignButton = ({name, handler, styles = defaultStyle }: Props) => {
  return (
    <SignBtn style={styles} onClick={handler}>{name}</SignBtn>
  )
}

export default SignButton
