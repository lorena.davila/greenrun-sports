import { createGlobalStyle } from 'styled-components';

  const GlobalStyle = createGlobalStyle`
 *,
   *::after,
   *::before {
    box-sizing: border-box;
    scroll-behavior: smooth;
    margin: 0;
    padding: 0;
  }
  body {
    background: ${({ theme }:any) =>  theme.body };
    color: ${({ theme }:any) =>  theme.text };
    p {
    color: ${({ theme }:any) =>  theme.paragraph };
    }
    transition: all 0.50s linear;
  }
  a {
    text-decoration: none;
  }
  li {
    list-style-type: none;
  }
  html {
  font-size: 62.5%;
  }
 
  @font-face {
  font-family: 'Epilogue';
  font-style: normal;
  font-weight: 400;
  src: url('./assets/fonts/epilogue-v11-latin-regular.eot'); /* IE9 Compat Modes */
  src: local(''),
       url('./assets/fonts/epilogue-v11-latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('./assets/fonts/epilogue-v11-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('./assets/fonts/epilogue-v11-latin-regular.woff') format('woff'), /* Modern Browsers */
       url('./assets/fonts/epilogue-v11-latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('./assets/fonts/epilogue-v11-latin-regular.svg#Epilogue') format('svg'); /* Legacy iOS */
}

@font-face {
  font-family: 'DM Sans Regular';
  src: url('./assetsfonts/dm-sans/DMSans-Regular.eot');
  src: local('./assets/fonts/DM Sans Regular'), local('DMSans-Regular'),
      url('./assets/fonts/DMSans-Regular.eot?#iefix') format('embedded-opentype'),
      url('./assets/fonts/DMSans-Regular.woff2') format('woff2'),
      url('./assets/fonts/DMSans-Regular.woff') format('woff'),
      url('./assets/fonts/DMSans-Regular.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
}

@font-face {
  font-family: 'DM Sans Bold';
  src: url('./assets/fonts/DMSans-Bold.eot');
  src: local('./assets/fonts/DM Sans Bold'), local('DMSans-Bold'),
      url('./assets/fonts/DMSans-Bold.eot?#iefix') format('embedded-opentype'),
      url('./assets/fonts/DMSans-Bold.woff2') format('woff2'),
      url('./assets/fonts/DMSans-Bold.woff') format('woff'),
      url('./assets/fonts/DMSans-Bold.ttf') format('truetype');
  font-weight: bold;
  font-style: normal;
}

`;

  export default GlobalStyle;