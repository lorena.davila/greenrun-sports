export default interface SavedSport {
  sportIndex: number,
  liked: boolean,
  date: Date,
  user: string,
}