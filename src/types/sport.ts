export default interface SportData {
  id?: string | null,
  name: string,
  icon: string,
  image: string,
}